package tm.info.bigbass1997.states;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import tm.info.bigbass1997.Debug;
import tm.info.bigbass1997.MainDisplay;
import tm.info.bigbass1997.managers.FontManager;
import tm.info.bigbass1997.managers.ImageManager;
import tm.info.bigbass1997.managers.ShapeManager;
import tm.info.bigbass1997.objects.Level;

public class GamePlayState extends BasicGameState {

	/*
	 * Breakout is a clone of the Atari game "Breakout".
	 * Copyright (C) 2013 Bigbass1997
	 * 
	 * This program is free software: you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License 
	 * as published by the Free Software Foundation, either version
	 * 3 of the License, or (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 * GNU General Public License for more details.
	 * 
	 * You should have received a copy of the GNU General Public
	 * License along with this program.
	 * If not, see <http://www.gnu.org/licenses/>
	 */
	
	public static Level level;
	public static Rectangle topBar, leftBar, rightBar;
	
	int stateID = -1;

	public GamePlayState(int stateID) {
		this.stateID = stateID;
	}
	
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		try {
			gc.setMouseCursor(ImageManager.getImage("resources/images/blankmouse.png"), 0, 0);
		} catch (SlickException e) {
			e.printStackTrace();
		}
		
		level = new Level(20, 80, 10, 10, 100, 20);
		topBar = new Rectangle(0.0f, 0.0f, (float) gc.getWidth(), 78.0f);
		leftBar = new Rectangle(0.0f, 0.0f, 18.0f, (float) gc.getHeight());
		rightBar = new Rectangle(gc.getWidth() - 20.0f, 0.0f, 20.0f, (float) gc.getHeight());
		
		Debug.logInfo("GamePlayState Initialization Method Loaded!");
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		g.fill(ShapeManager.Rect(g, 0, 0, MainDisplay.SWIDTH, MainDisplay.SHEIGHT, 0xFFFFFFFF)); // Background
		
		level.render(gc, sbg, g);
		
		g.setColor(new Color(0xFF000000));
		g.fill(topBar);
		g.fill(leftBar);
		g.fill(rightBar);
		
		//COPYRIGHT CREDITS\\
		ShapeManager.String(g, "Copyright (C) 2013 Bigbass1997", 10, MainDisplay.SHEIGHT - 24, 0xFFFFFFFF, FontManager.PK22);
		ShapeManager.String(g, "GNU General Public License", MainDisplay.SWIDTH - 240,  MainDisplay.SHEIGHT - 24, 0xFFFFFFFF, FontManager.PK22);
		
		//MOUSE IMAGE CONTROLLER\\
		g.drawImage(ImageManager.getImage(2), MainMenuState.pointerx, MainMenuState.pointery);
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		level.update(gc, sbg, delta);
		
		MainMenuState.pointerx = MainMenuState.mouseX;
		MainMenuState.pointery = MainMenuState.mouseY;
		
		MainMenuState.mouseX = MainDisplay.appGC.getInput().getMouseX();
		MainMenuState.mouseY = MainDisplay.appGC.getInput().getMouseY();
	}
	
	@Override
	public int getID() {
		return stateID;
	}
}
