package tm.info.bigbass1997.objects;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;

public class Block {

	/*
	 * Breakout is a clone of the Atari game "Breakout".
	 * Copyright (C) 2013 Bigbass1997
	 * 
	 * This program is free software: you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License 
	 * as published by the Free Software Foundation, either version
	 * 3 of the License, or (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 * GNU General Public License for more details.
	 * 
	 * You should have received a copy of the GNU General Public
	 * License along with this program.
	 * If not, see <http://www.gnu.org/licenses/>
	 */
	
	private int x, y, width, height;
	private int type;// 1 = NORMAL, 2 = FAST, 3 = SLOW
	
	private int color;
	
	private Line topHitBox, bottomHitBox, leftHitBox, rightHitBox;
	private Rectangle totalHitBox;
	
	private boolean isVisable;
	
	public Block(int x, int y, int width, int height, int type) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.type = type;
		
		isVisable = true;
		
		topHitBox = new Line(x, y, x + width, y);
		bottomHitBox = new Line(x, y + height, x + width, y + height);
		leftHitBox = new Line(x, y, x, y + height);
		rightHitBox = new Line(x + width, y, x + width, y + height);
		
		totalHitBox = new Rectangle(x, y, width, height);

		switch(type){
		case 1:
			color = 0xFFCDCDCD;
			break;
		case 2:
			color = 0xFFFF1B0A;
			break;
		case 3:
			color = 0xFF37F0F0;
			break;
		default:
			color = 0xFF000000;
			break;
		}
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException { // Object Render
		if(isVisable){
			g.setColor(new Color(color));
			g.fill(new Rectangle(x, y, width, height));
		}
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException { // Object Update
		if(isVisable){
			topHitBox = new Line(x, y, x + width, y);
			bottomHitBox = new Line(x, y + height, x + width, y + height);
			leftHitBox = new Line(x, y, x, y + height);
			rightHitBox = new Line(x + width, y, x + width, y + height);
		}
	}
	
	public Line getTopHitBox(){return topHitBox;}
	public Line getBottomHitBox(){return bottomHitBox;}
	public Line getLeftHitBox(){return leftHitBox;}
	public Line getRightHitBox(){return rightHitBox;}
	
	public Rectangle getTotalHitBox(){return totalHitBox;}
	
	public boolean isVisable(){return isVisable;}
	public void setVisable(boolean bool){isVisable = bool;}
	
	public String getType(){
		switch(type){
		case 1:
			return "NORMAL";
		case 2:
			return "FAST";
		case 3:
			return "SLOW";
		default:
			return "ERROR";
		}
	}
}
