package tm.info.bigbass1997.objects;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;

public class Object {
	
	/*
	 * Breakout is a clone of the Atari game "Breakout".
	 * Copyright (C) 2013 Bigbass1997
	 * 
	 * This program is free software: you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License 
	 * as published by the Free Software Foundation, either version
	 * 3 of the License, or (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 * GNU General Public License for more details.
	 * 
	 * You should have received a copy of the GNU General Public
	 * License along with this program.
	 * If not, see <http://www.gnu.org/licenses/>
	 */
	
	// ANIMATION REFERANCE http://bit.ly/179YYFF
	//new Object(50, 50, 16, 16, new Image[]{new Image("resources/images/anim1.png"), new Image("resources/images/anim2.png")}, new int[]{200, 200});
	
	private int x, y, width, height;
	
	private int[] animDurations;
	private Image[] animImages;
	private Animation anim;
	
	private Rectangle hitBox;
	
	public Object(int x, int y, int width, int height, Image[] animImages, int[] animDurations){ // Object Initialization
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		
		this.animImages = animImages;
		this.animDurations = animDurations;
		
		anim = new Animation(this.animImages, this.animDurations, false);
		
		hitBox = new Rectangle(x, y, width, height);
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException { // Object Render
		g.setColor(new Color(0xFF00FF00));
		g.fill(hitBox);
		
		anim.draw(x, y);
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException { // Object Update
		hitBox.setBounds(x, y, width, height);
		
		anim.update(delta);
	}
	
	public int getX(){return x;}
	public void setX(int x){this.x = x;}
	
	public int getY(){return y;}
	public void setY(int y){this.y = y;}
	
	public int getWidth(){return width;}
	public void setWidth(int width){this.width = width;}
	
	public int getHeight(){return height;}
	public void setHeight(int height){this.height = height;}
	
	public Rectangle getHitBox(){return hitBox;}
}
