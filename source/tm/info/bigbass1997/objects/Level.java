package tm.info.bigbass1997.objects;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.state.StateBasedGame;

public class Level {

	/*
	 * Breakout is a clone of the Atari game "Breakout".
	 * Copyright (C) 2013 Bigbass1997
	 * 
	 * This program is free software: you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License 
	 * as published by the Free Software Foundation, either version
	 * 3 of the License, or (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 * GNU General Public License for more details.
	 * 
	 * You should have received a copy of the GNU General Public
	 * License along with this program.
	 * If not, see <http://www.gnu.org/licenses/>
	 */
	
	@SuppressWarnings("unused")
	private int x, y, rows, columns, blockWidth, blockHeight;
	private Block[][] stage;
	
	private Ball ball;
	private Paddle paddle;
	
	private boolean moveUp, moveDown, moveLeft, moveRight;
	private float upSpeed, downSpeed, leftSpeed, rightSpeed;
	
	private Line topBorder, bottomBorder, leftBorder, rightBorder;
	
	public Level(int x, int y, int rows, int columns, int blockWidth, int blockHeight){
		this.x = x;
		this.y = y;
		this.rows = rows;
		this.columns = columns;
		this.blockWidth = blockWidth;
		this.blockHeight = blockHeight;
		
		this.stage = new Block[rows][columns];
		
		// Populate 3D Block Array \\
		for(int r = 0; r < rows; r++){
			for(int c = 0; c < columns; c++){
				stage[r][c] = new Block(((blockWidth + 2) * c) + x, ((blockHeight + 2) * r) + y, blockWidth, blockHeight, 1);
			}
		}
		for(int c = 0; c < columns; c++){
			stage[3][c] = new Block(((blockWidth + 2) * c) + x, ((blockHeight + 2) * 3) + y, blockWidth, blockHeight, 3);
		}
		for(int c = 2; c < 8; c++){
			stage[1][c] = new Block(((blockWidth + 2) * c) + x, ((blockHeight + 2) * 1) + y, blockWidth, blockHeight, 2);
		}
		//--------------------------\\
		
		this.ball = new Ball(530, 500, 5);
		this.paddle = new Paddle(490, 550, 280, 10);
		
		topBorder = new Line(x, y, x + 1024, y);
		bottomBorder = new Line(x, 595, x + 1024, 795);
		leftBorder = new Line(x, y, x, 800);
		rightBorder = new Line(x + 1024, y, x + 1024, 795);
		
		moveUp = true; moveDown = false;
		moveLeft = true; moveRight = false;
		
		upSpeed = 2.0f; downSpeed = 2.0f;
		leftSpeed = 2.0f; rightSpeed = 2.0f;
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException { // Object Render
		for(int r = 0; r < rows; r++){
			for(int c = 0; c < columns; c++){
				stage[r][c].render(gc, sbg, g);
			}
		}
		
		ball.render(gc, sbg, g);
		paddle.render(gc, sbg, g);
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException { // Object Update
		if(moveUp) ball.setY((int) (ball.getY() - upSpeed));
		if(moveDown) ball.setY((int) (ball.getY() + downSpeed));
		if(moveLeft) ball.setX((int) (ball.getX() - leftSpeed));
		if(moveRight) ball.setX((int) (ball.getX() + rightSpeed));
		
		//Blocks Hit-Detection
		for(int r = 0; r < rows; r++){
			for(int c = 0; c < columns; c++){
				stage[r][c].update(gc, sbg, delta);
				if(stage[r][c].isVisable()){
					if(ball.getHitBox().intersects(stage[r][c].getTopHitBox())){
						moveDown = true;
						moveUp = false;
					}
					
					if(ball.getHitBox().intersects(stage[r][c].getBottomHitBox())){
						moveUp = false;
						moveDown = true;
					}
					
					if(ball.getHitBox().intersects(stage[r][c].getLeftHitBox())){
						moveLeft = false;
						moveRight = true;
					}
					
					if(ball.getHitBox().intersects(stage[r][c].getRightHitBox())){
						moveRight = false;
						moveLeft = true;
					}
					
					if(ball.getHitBox().intersects(stage[r][c].getTopHitBox()) && stage[r][c].getType() == "FAST"){
						upSpeed += 0.5f;
						downSpeed += 0.5f;
						leftSpeed += 0.5f;
						rightSpeed += 0.5f;
					}
					
					if(ball.getHitBox().intersects(stage[r][c].getTotalHitBox())){
						stage[r][c].setVisable(false);
					}
				}
			}
		}
		
		//Left Wall Hit-Detection
		if(ball.getHitBox().intersects(leftBorder)){
			moveLeft = false;
			moveRight = true;
		}
		
		//Right Wall Hit-Detection
		if(ball.getHitBox().intersects(rightBorder)){
			moveRight = false;
			moveLeft = true;
		}
		
		//Paddle Hit-Detection
		if(ball.getHitBox().intersects(paddle.getHitBox())){
			moveDown = false;
			moveUp = true;
			
			if(moveRight){
				moveLeft = true;
				moveRight = false;
			}else if(moveLeft){
				moveRight = true;
				moveLeft = false;
			}
			
			if(ball.getHitBox().intersects(paddle.getLeft())){
				leftSpeed += 0.25f;
				rightSpeed += 0.25f;
			}
			if(ball.getHitBox().intersects(paddle.getLeftMid())){
				leftSpeed += 0.15f;
				rightSpeed += 0.15f;
			}
			if(ball.getHitBox().intersects(paddle.getMid())){
				leftSpeed -= 0.1f;
				rightSpeed -= 0.1f;
			}
			if(ball.getHitBox().intersects(paddle.getRightMid())){
				leftSpeed += 0.15f;
				rightSpeed += 0.15f;
			}
			if(ball.getHitBox().intersects(paddle.getRight())){
				leftSpeed += 0.25f;
				rightSpeed += 0.25f;
			}
		}
		
		paddle.update(gc, sbg, delta);
		ball.update(gc, sbg, delta);
	}
}
