package tm.info.bigbass1997.objects;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.state.StateBasedGame;

public class Ball {

	private int x, y, radius;
	
	private Circle hitBox;
	
	public Ball(int x, int y, int radius) {
		this.x = x;
		this.y = y;
		this.radius = radius;
		
		hitBox = new Circle(x - radius, y - radius, radius);
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException { // Object Render
		g.setColor(new Color(0xFF000000));
		g.fill(hitBox);
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException { // Object Update
		hitBox = new Circle(x - radius, y - radius, radius);
	}

	public Circle getHitBox(){return hitBox;}
	public int getX(){return x;}
	public void setX(int x){this.x = x;}
	
	public int getY(){return y;}
	public void setY(int y){this.y = y;}
}
