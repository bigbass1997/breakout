package tm.info.bigbass1997.objects;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;

public class Paddle {
	
	private float x, y, width, height;
	
	private Rectangle hitBox;
	
	private Line bound1, bound2, bound3, bound4, bound5;
	
	public Paddle(float x, float y, float width, float height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		
		bound1 = new Line(x, y, x + (width * 0.05f), y); // 5% of width
		bound2 = new Line(x + (width * 0.05f), y, x + (width * 0.25f), y); // 20% of width
		bound3 = new Line(x + (width * 0.25f), y, x + (width * 0.75f), y); // 50% of width
		bound4 = new Line(x + (width * 0.75f), y, x + (width * 0.95f), y); // 20% of width
		bound5 = new Line(x + (width * 0.95f), y, x + width, y); // 5% of width
		
		hitBox = new Rectangle(x, y, width, height);
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException { // Object Render
		//g.drawImage(new Image("resources/images/paddle.png"), x, y);
		g.setColor(new Color(0xFF301830));
		//g.fill(hitBox);
		
		g.setColor(new Color(0xFFFF0000));
		g.draw(bound1);
		g.setColor(new Color(0xFF0000FF));
		g.draw(bound2);
		g.setColor(new Color(0xFF00FF00));
		g.draw(bound3);
		g.setColor(new Color(0xFF0000FF));
		g.draw(bound4);
		g.setColor(new Color(0xFFFF0000));
		g.draw(bound5);
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException { // Object Update
		Input input = gc.getInput();
		
		x = input.getMouseX() - (width / 2);
		
		if(x < 20){
			x = 20;
		}
		if(x > (gc.getWidth() - width) - 22){
			x = (gc.getWidth() - width) - 22;
		}
		
		hitBox = new Rectangle(x, y, width, height);

		bound1 = new Line(x, y, x + (width * 0.05f), y);
		bound2 = new Line(x + (width * 0.05f), y, x + (width * 0.25f), y);
		bound3 = new Line(x + (width * 0.25f), y, x + (width * 0.75f), y);
		bound4 = new Line(x + (width * 0.75f), y, x + (width * 0.95f), y);
		bound5 = new Line(x + (width * 0.95f), y, x + width, y);
	}

	public Rectangle getHitBox(){return hitBox;}
	
	public Line getLeft(){return bound1;}
	public Line getLeftMid(){return bound2;}
	public Line getMid(){return bound3;}
	public Line getRightMid(){return bound4;}
	public Line getRight(){return bound5;}
}
